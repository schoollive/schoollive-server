const express = require('express');
const router = express.Router();
const config = require('../../config/security');
const expressMiddleWare = require(config.LibraryDir + '/middleware');

const {createReportForm} = require('../../controller/express/api/admin/dashboard/create_reportForm');

router.post('/report_form/create_new', expressMiddleWare.expressMiddleware, createReportForm);

module.exports = router;
