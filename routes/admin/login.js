const express = require('express');
const router = express.Router();
const config = require('../../config/security');

const {AdminLogin} = require('../../controller/express/api/admin/login_admin');
router.post('/login', AdminLogin);

module.exports = router;
