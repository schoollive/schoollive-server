const express = require('express');
const router = express.Router();
const config = require('../../config/security');
const expressMiddleWare = require(config.LibraryDir + '/middleware');

const {createNewAdmin} = require('../../controller/express/api/admin/create_admin');

const {
    addNewStudent,
    addNewClass,
    addNewSubject
} = require('../../controller/express/api/admin/user/add_new');

const { createNewSemester} = require('../../controller/express/api/admin/dashboard/create_semester');

router.post('/create_new', createNewAdmin);
router.post('/class/add_new', expressMiddleWare.expressMiddleware, addNewClass);
router.post('/class/create_new_semester', expressMiddleWare.expressMiddleware, createNewSemester);
router.post('/student/add_new', expressMiddleWare.expressMiddleware, addNewStudent);
router.post('/subject/add_new', expressMiddleWare.expressMiddleware, addNewSubject);

module.exports = router;