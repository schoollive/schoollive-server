const express = require('express');
const router = express.Router();
const config = require('../../../config/security');
const expressMiddleWare = require(config.LibraryDir + '/middleware');

const {
    showInFoUser
} = require('../../../controller/express/api/user/login')

const {
    postNewStory,
    showStory,
    showMyHome
} = require('../../../controller/express/api/user/story_action')
const {
    commentStory,
    countCommnet,
    showComment
} = require('../../../controller/express/api/user/comment_action')

const {
    reactionStory
} = require('../../../controller/express/api/user/reaction_action')

router.post('/show_info', expressMiddleWare.expressMiddleware, showInFoUser);

router.post('/post_new', expressMiddleWare.expressMiddleware, postNewStory);
router.post('/show_story_of_class', expressMiddleWare.expressMiddleware, showStory);
router.post('/my_home', expressMiddleWare.expressMiddleware, showMyHome);

router.post('/comment', expressMiddleWare.expressMiddleware,commentStory);
router.post('/comment/count', expressMiddleWare.expressMiddleware, countCommnet);
router.post('/comment/show', expressMiddleWare.expressMiddleware, showComment);

router.post('/reaction', expressMiddleWare.expressMiddleware, reactionStory);

module.exports = router;