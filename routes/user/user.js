const express = require('express');
const router = express.Router();
const config = require('../../config/security');
const multer = require('multer');
const expressMiddleWare = require(config.LibraryDir + '/middleware');
const path = require('path');

//Common
router.use('/story', require('../user/common/index'));
//Teacher
router.use('/teacher', require('../user/teacher/index'));


//Login
const userLogin = require('../../controller/express/api/user/login');
//Check type, email and phone
const checkEmailAndPhone = require('../../controller/express/api/user/register');
//Register Teacher
const checkCodeClass = require('../../controller/express/api/user/register');
const registerNewTeacher = require('../../controller/express/api/user/register');
const showListSubject = require('../../controller/express/api/user/subject_action');
//Register Parent
const checkCodeStudent = require('../../controller/express/api/user/register');
const registerNewParent = require('../../controller/express/api/user/register');

//Login API
router.post('/login', userLogin.userLogin);
//Check type, email and phone API
router.post('/register/check_email_and_phone', checkEmailAndPhone.checkEmailAndPhone);
//Register Teacher API
router.post('/register/check_code_class', checkCodeClass.checkCodeClass);
router.post('/register/teacher', registerNewTeacher.registerNewTeacher);
router.post('/register/teacher/show_list_subject', showListSubject.showListSubject);
//Register Parent API
router.post('/register/check_code_student', checkCodeStudent.checkCodeStudent);
router.post('/register/parent', registerNewParent.registerNewParent);



const uploadAvatar = require('../../controller/express/api/user/upload_avatar')

//Show list class
const showAllMyClass = require('../../controller/express/api/user/teacher_action');
//Show list student
const showAllStudentOfClass = require('../../controller/express/api/user/teacher_action');


//Upload Avatar
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
    const dest = path.join('public/uploads/avatars')
      cb(null, dest)
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname)
    }
  })
var multerAvatar = multer({ storage: storage })
router.post('/upload_avatar', [expressMiddleWare.expressMiddleware, multerAvatar.single('avatar')], uploadAvatar.uploadAvatar)
//Show list Class
router.post('/teacher/show_list_class', expressMiddleWare.expressMiddleware, showAllMyClass.showAllMyClass);
//Show list Student
router.post('/teacher/show_list_student', expressMiddleWare.expressMiddleware, showAllStudentOfClass.showAllStudentOfClass);



module.exports = router;