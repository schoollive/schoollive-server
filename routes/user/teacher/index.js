const express = require('express');
const router = express.Router();
const config = require('../../../config/security');
const expressMiddleWare = require(config.LibraryDir + '/middleware');

const {
    areYouHomeRoomTeacher, 
    showAllMyClass, 
    studentPlanner, 
    showStudentPlanner,
    reportValue
} = require('../../../controller/express/api/user/teacher_action')

const {
    showAllReporstForms,
    showAllReporstPersonality
} = require('../../../controller/express/api/user/form_action')


router.post('/check_homeroom' , expressMiddleWare.expressMiddleware, areYouHomeRoomTeacher);
router.post('/create_studentplanner', expressMiddleWare.expressMiddleware, studentPlanner);
router.post('/show_studentplanner', expressMiddleWare.expressMiddleware, showStudentPlanner)//Report Value Personality
router.post('/report_personality_value', expressMiddleWare.expressMiddleware, reportValue);
router.post('/show_report_personality_value', expressMiddleWare.expressMiddleware, showAllReporstPersonality);
router.post('/show_report_forms', expressMiddleWare.expressMiddleware, showAllReporstForms);
router.post('/show_list_class', expressMiddleWare.expressMiddleware, showAllMyClass);

module.exports = router;