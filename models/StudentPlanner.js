const mongoose = require('mongoose');

const studentPlannerSchema = new mongoose.Schema({
    timeTables: 
    [{
        dateOfWeek: 
        {
            type: String
        },
        timeTable:
        [{
            subjectId:
            {
                type: mongoose.Schema.Types.ObjectId, 
                ref: 'Subject'
            },
            AreasOfLearning: 
            {
                type: String
            },
        }]
    }],
    weekOfSchoolLiveYear:
    {
        type: Number
    },
    teacherId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    classId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Class'
    }
}, {timestamps: true})

module.exports = mongoose.model('StudentPlanner', studentPlannerSchema);