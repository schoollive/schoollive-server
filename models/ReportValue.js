const mongoose = require('mongoose');

const reportValueSchema = new mongoose.Schema({
    value: 
    {
        type: Number
    },
    group: 
    {
        type: Number
    },
    commentOfTeacher:
    {
        type: String
    },
    className: 
    {
        type: String
    },
    teacherName:
    {
        type: String
    },
    studentName:
    {
        type: String
    },
    schoolYear: 
    {
        type: String
    },
    semester: 
    {
        type: Number
    },
    reportFormName:
    {
        type: String
    },
    reportFormGroup:
    {
        type: String
    },
    reportFormId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'ReportForm'
    },
    studentId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Student'
    },
    teacherId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Teacher'
    },
    classId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Class'
    }
}, {timestamps: true})

module.exports = mongoose.model('ReportValue', reportValueSchema);