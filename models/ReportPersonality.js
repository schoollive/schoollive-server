const mongoose = require('mongoose');

const reportPersonalitySchema = new mongoose.Schema({
    className: 
    {
        type: String
    },
    teacherName:
    {
        type: String
    },
    studentName:
    {
        type: String
    },
    schoolYear: 
    {
        type: String
    },
    semester: 
    {
        type: Number
    },
    studentId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Student'
    },
    teacherId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Teacher'
    },
    classId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Class'
    },
}, {timestamps: true})

module.exports = mongoose.model('ReportPersonality', reportPersonalitySchema);