const mongoose = require('mongoose');
const validator = require('validator');

const teacherSchema = new mongoose.Schema ({
    userId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    subject: 
    {
        type: String
    },
    homeRoomTeacherCode:
    {
        type: String,
        default: null
    },
    classCode:
    {
        type: Array,
        uppercase: true,
        trim: true,
        index: true
    },
    subjectId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Subject"
    }
}, {timestamps: true})

module.exports = mongoose.model('Teacher', teacherSchema);