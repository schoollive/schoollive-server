const mongoose = require('mongoose');

const ChatSchema = new mongoose.Schema({
    sendID: 
    {
        type: Schema.Types.ObjectId, ref: 'User'
    },
    receiverID:
    {
        type: Schema.Types.ObjectId, ref: 'User'
    },
    content:
    {
        type: String,
        trim: true
    },
    isSeen: 
    {
        type: Boolean,
        default: false
    },
    createdDate: 
    {
        type: Date,
        default: Date(Date.now())
    },
});

ChatSchema.pre('save', next => {
    if(this.isNew || this.Modified) {
        this.dateUpdated = Date(Date.now());
    }
    return next;
})

module.exports = mongoose.model('Chat', ChatSchema);