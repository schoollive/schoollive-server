const mongoose = require('mongoose');

const storySchema = new mongoose.Schema({
    content:
    {
        type: String
    },
    reactionNumbers:
    {
        type: Number,
        default: 0
    },
    commentNumbers:
    {
        type: Number,
        default: 0
    },
    userId: 
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    classId: 
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Class'
    },
}, {timestamps: true});
module.exports = mongoose.model('Story', storySchema);