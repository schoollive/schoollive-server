const mongoose = require('mongoose');
const validator = require('validator');

const parentSchema = new mongoose.Schema({
    studentId:
        [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "Student"
            }
        ],
    userId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    job:
    {
        type: String,
    },
    studentCode:
    {
        type: Array,
        uppercase: true,
        trim: true,
        index: true
    },
}, { timestamps: true });

module.exports = mongoose.model('Parent', parentSchema);