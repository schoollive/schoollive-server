const mongoose = require('mongoose');
const validator = require('validator');
const randomstring = require("randomstring");

const schoolSchema = new mongoose.Schema({
    name: 
    {
        type: String,
        require: true,
    },
    username:
    {
        type: String,
        lowercase: true,
        match: [/^[a-zA-Z0-9]+$/, 'Chỉ bao gồm ký tự a-Z, 0-9'],
        index: true,
        require: true,
    },
    email:
    {
        type: String, trim: true, require: true,
        index: {unique: true},
        validate: 
        {
            validator: validator.isEmail
        }
    },
    address:{
        type: String,
    },
    password:
    {
        type: String,
        require: true,
    },
    phone:
    {
        type: String,
        index: {unique: true},
        validate:
        {
            validator: validator.isMobilePhone
        }
    },
    numberOfClasses:
    {
        type: Number
    },
    numberOfTeachers: 
    {
        type: Number
    },
    description:
    {
        type: String
    },
    schoolCode:
    {
        type: String,
        uppercase: true,
        unique: true,
        trim: true,
        index: true
    },
    adminId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Admin"
    },
}, {timestamps: true});

schoolSchema.methods.randomCode = function() {
    this.schoolCode = randomstring.generate({
        length: 8,
        charset: 'alphabetic'
    })
  
}

module.exports = mongoose.model('School', schoolSchema);