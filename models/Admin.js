const mongoose = require('mongoose');
const validator = require('validator');

const adminSchema = new mongoose.Schema({
    userType:
    {
        type: String,
        default: 0
    },
    name:
    {
        type: String,
        require: true
    },
    username:
    {
        type: String,
        lowercase: true,
        match: [/^[a-zA-Z0-9]+$/, 'Chỉ bao gồm ký tự a-Z, 0-9'],
        index: true,
        require: true,
    },
    email:
    {
        type: String, trim: true, require: true,
        index: {unique: true},
        validate: 
        {
            validator: validator.isEmail
        }
    },
    password:
    {
        type: String,
        require: true,
    }
}, {timestamps: true});

module.exports = mongoose.model('Admin', adminSchema);