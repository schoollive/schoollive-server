const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
    miniTest: 
    {
        type: Number
    },
    bigTest:
    {
        type: Number
    },
    commentOfTeacher:
    {
        type: String
    },
    subjedtId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subject' 
    },
    studentId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Student'
    }
}, {timestamps: true})

module.exports = mongoose.model('Review', reviewSchema);