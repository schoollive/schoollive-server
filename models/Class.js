const mongoose = require('mongoose');
const randomstring = require('randomstring');

const classSchema = new mongoose.Schema({
    name:
    {
        type: String,
    },
    startDate:
    {
        type: Date,
    },
    endDate:
    {
        type:Date
    },
    numberOfTeachers:
    {
        type: Number
    },
    numberOfStudents:
    {
        type: Number
    },
    description:
    {
        type: String
    },
    classCode:
    {
        type: String,
        uppercase: true,
        unique: true,
        trim: true,
        index: true
    },
    schoolId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "School"
    }
})

classSchema.methods.randomCode = function() {
    this.classCode = randomstring.generate({
        length: 8,
        charset: 'alphabetic'
    })
}

module.exports = mongoose.model('Class', classSchema);

