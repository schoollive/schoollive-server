const mongoose = require('mongoose');

const reactionSchema = new mongoose.Schema({
    isReaction:
    {
        type: Boolean,
        default: false
    },
    userId: 
    { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User' 
    },
    storyId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Story'
    }
})
module.exports = mongoose.model('Reaction', reactionSchema);