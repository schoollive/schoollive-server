const mongoose = require('mongoose');

const semesterSchema = new mongoose.Schema({
    semester: 
    {
        type: Number
    },
    schoolOfYear:
    {
        type: String
    },
    classId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class"
    }
}, {timestamps: true})

module.exports = mongoose.model('Semester', semesterSchema);