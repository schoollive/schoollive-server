const mongoose = require('mongoose');
const randomstring = require('randomstring');

const studentSchema = new mongoose.Schema({
    userType:
    {
        type: String,
        default: 3
    },
    name:
    {
        type: String,
        require: true
    },
    gender:
    {
        type: Number,
        default: 1
    },
    dateOfBirth:
    {
        type: Date,
    },
    skill:
    {
        type: String
    },
    subjectClass:
    {
        type: String
    },
    studentCode:
    {
        type: String,
        uppercase: true,
        unique: true,
        trim: true,
        index: true
    },
    classId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Class'
    },
}, {timestamps: true});

studentSchema.methods.randomCode = function() {
    this.studentCode = randomstring.generate({
        length: 8,
        charset: 'alphabetic'
    })
}

module.exports = mongoose.model('Student', studentSchema);
