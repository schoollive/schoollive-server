const mongoose = require('mongoose');
const validator = require('validator');

const userSchema = new mongoose.Schema({
    userType:
    {
        type: String
    },
    name:
    {
        type: String,
        require: true
    },
    username:
    {
        type: String,
        lowercase: true,
        match: [/^[a-zA-Z0-9]+$/, 'Chỉ bao gồm ký tự a-Z, 0-9'],
        index: true,
        default: null
    },
    avatar:
    {
        type: String,
        default: null
    },
    email:
    {
        type: String, trim: true, require: true,
        unique: true,
        validate: 
        {
            validator: validator.isEmail
        }
    },
    password:
    {
        type: String,
        require: true
    },
    phone:
    {
        type: String,
        unique: true,
        validate:
        {
            validator: validator.isMobilePhone
        },
        required: true
    },
    gender: 
    {
        type: Number,
        require: true
    },
    dateOfBirth:
    {
        type: Date,
        require: true
    },
    address:
    {
        type: String,
        require: true
    },
    description:
    {
        type: String,
        default: null
    },
    statusID:
    {
        type: Number,
        default: 1
    },
    socketID:
    {
        type: String,
        trim: true
    },
    personInbox: [String]
});

module.exports = mongoose.model('User', userSchema);