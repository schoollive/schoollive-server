const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
    content: 
    {
        type: String
    },
    userId:
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User' 
    },
    storyId:
    {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Story'
    }
}, {timestamps: true})

module.exports = mongoose.model('Comment', commentSchema);