const mongoose = require('mongoose');

const subjectSchema = new mongoose.Schema({
    name: 
    {
        type: String
    },
    description:
    {
        type: String
    },
}, {timestamps: true})

module.exports = mongoose.model('Subject', subjectSchema);