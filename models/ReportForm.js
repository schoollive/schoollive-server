const mongoose = require('mongoose');

const reportFormSchema = new mongoose.Schema({
    name: 
    {
        type: String
    },
    group:
    {
        type: String
    }
}, {timestamps: true})

module.exports = mongoose.model('ReportForm', reportFormSchema);