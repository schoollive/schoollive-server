const http = require('http');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors')

//Create global
const app = express();

//Middle ware
app.use(cors())
app.use(express.json());

//Setup bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Models
require('./models/User');
require('./models/Parent');
require('./models/Student');

// Use static files
app.use(express.static('public'))


//Import Routes
const routes = require('./routes');
//Route Middlewares
app.use('/api', routes);

//Connect to DB
const Data = require('./config/mongdb');
mongoose.connect(Data.MongoURI, { useNewUrlParser: true }, () => {
    console.log("Connected DB");
});

app.listen(8080, () => {
    console.log("Server started on port 8080")
})
