const jwt = require('jsonwebtoken');
const config = require('../config/security');
const send = require(config.LibraryDir + '/send')
const User = require(config.ModelDir +  '/User');

exports.expressMiddleware = (req, res, next) => {
    let token = req.headers['access-token'];
    if(token) {
        jwt.verify(token, config.secret, async (err, decoded) => {
            if(err){
                console.log(err)
                return send.error (res, "Failed to authenticate token.")
            }
            req.tokenInfo = decoded;
            const user = await User.findOne({_id: req.tokenInfo._id}, '-password')
            req.user = user || null
            next();
        });
    } else {
        return send.fail(res, "No token provided.");
    }
}
exports.socketMiddleware = (socket, next) => {
    let token = socket.handshake.query.token;
    if(token === undefined || token === null){
        next(new Error('No token provided.'));
    } else {
        jwt.verify(token, config.secret, (err, decoded) => {
            if(err) {
                return (err);
            }
            socket.handshake.decodedToken = decoded;
        });
        next();
    }
}