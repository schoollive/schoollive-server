const config = require("../../config/security");
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const Chat = require(config.ModelDir + '/Chat');
const Parent = require(config.ModelDir + '/Parent');
const Teacher = require(config.ModelDir + '/Teacher');

module.exports = (io) => {
    io.use(require(config.LibraryDir + '/middleware').socketMiddleware);

    io.on('connection', function(socket){
        let decodedToken = socket.handshake.decodedToken;
        if(!decodedToken){
            return;
        }
        console.log(decodedToken.email + " has been connected to socket server with socket ID: " + socket.id);
        User.findOne({_id: decodedToken._id})
        .then(sender => {
            sender.socketID = socket.id;
            sender.save();
        })
        .catch(err => {
            return send.responseSocket(socket, err)
        })
        
        /**
         * data:
         *  senderID
         *  content
         *  receiverID
         */
        socket.on('chatMessage', data => {
            Chat.create(data)
            User.findOne({_id: data.receiverID})
            .then(receiver => {
                if(receiver.socketID){
                    var input = {
                        senderID: data.senderID,
                        receiverID: data.receiverID,
                        content: data.content
                    }
                    socket.to(receiver.socketID).emit('chatMessage', input);
                }
            })
            .catch(err => response_socketio(socket, err))
        })

        socket.on('firstMessage', data => {
            Chat.crate(data);
            User.findOne({_id: data.receiverID})
            .then(receiver => {
                receiver.personInbox.push(data.senderID);
                receiver.save();
                if(receiver.socketID){
                    var input = {
                        senderID: data.senderID,
                        receiverID: data.receiverID,
                        content: data.content
                    }
                    socket.to(receiver.socketID).emit('firstMessage', input)
                }
                socket.emit('returnFriend')
            })
            .catch(err => responseSocket(socket, err))
            User.findOne({_id: data.senderID})
            .then(sender => {
                sender.personInbox.push(data.receiverID)
                sender.save();
            })
            .catch(err => responseSocket(socket, err));
        })

        /**
         * data:
         *  messageID
         *  receiverID
         */
        socket.on('isSeen', data => {
            let messageListNameSenderID = data.senderID + data.receiverID;
            let messageListNameReceiverID = data.receiverID + data.senderID;
            Chat.updateMany({ $or: [{messageListName: messageListNameSenderID}, {messageListName: messageListNameReceiverID}]}, { $set: { isSeen: true}})
            .catch(err => responseSocket(socket, err));
            User.findOne({_id: data.senderID})
            .then( sender => {
                if(sender.socketID){
                    socket.to(sender.socketID).emit('isSeen');
                }
            })
            .catch(err => response_socketio(socket, err))
        })
        
        socket.on('typing', data => {
            User.findOne({_id: data.receiverID})
            .then(sender => {
                if(sender.socketID){
                    if(data.isTyping == true){
                        socket.to(sender.socketID).emit('typing', {senderID: data.senderID ,isTyping: true});
                    } else {
                        socket.to(sender.socketID).emit('typing', {senderID: data.senderID ,isTyping: false});
                    }
                }
            })
            .catch(err => responseSocket(socket, err));
        })

        socket.on('disconect', () => {
            User.findOne({socketID: socket.id})
            .then(user => {
                user.socketID = '';
                user.save();
            })
            .catch(err => responseSocket(socket, err))
            console.log(decodedToken.email + " has disconected")
        })
    })
}
