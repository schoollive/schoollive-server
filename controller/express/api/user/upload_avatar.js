const mongoose = require('mongoose');
const multer = require('multer');
const config = require('../../../../config/security');
const CheckPassword = require(config.LibraryDir + '/password');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

exports.uploadAvatar = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    const file = req.file;
    console.log(file);
    try {
        user.avatar = 'uploads/avatars/' + file.filename;
        user.save();
        return send.success(res, "SUCCESSFUL", user);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}