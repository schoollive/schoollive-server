const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const moment = require('moment');
const config = require('../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const ComparePassword = require(config.LibraryDir + '/password');
const send = require(config.LibraryDir + '/send');

const User = require(config.ModelDir + '/User');
const Parent = require(config.ModelDir + '/Parent');
const Teacher = require(config.ModelDir +  '/Teacher');
const Student = require(config.ModelDir +  '/Student');
const Subject = require(config.ModelDir +  '/Subject');
const Class = require(config.ModelDir +  '/Class');


exports.checkEmailAndPhone = async (req, res, next) => {
    if(req.body.userType === "1" || req.body.userType === "2") {
        let missField = Common.checkMissParams(res, req.body, ['userType', 'email', 'phone']);
        if(missField)
            console.log("MISS_PARAMS");
        try {
            const {email, phone} = req.body;
            const checkUserEmail = await User.findOne({email})
            if (checkUserEmail){
                return send.fail(res, "EMAIL_EXISTS");
            }
            const checkUserPhone = await User.findOne({phone})
            if(checkUserPhone){
                return send.fail(res, "PHONE_EXISTS")
            } else {
                return send.success(res, "VALID");
            }
        } catch (e) {
            console.log(e) 
            return send.fail(res, "SOME_THING_WRONG")
        }
    } 
}

exports.checkCodeClass = async(req, res, next) => {
    let missField = Common.checkMissParams(res, req.body, ['classCode']);
    if(missField) 
        console.log("MISS_PARAMS");
    try {
        const {classCode} =  req.body;
        const checkCodeClass = await Class.findOne({classCode});
        if(!checkCodeClass) {
            return send.fail(res, "UNVALID");
        } else {
            return send.success(res, "VALID");
        }
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG")
    }
}

exports.checkCodeStudent = async (req, res, next) => {
    let missField = Common.checkMissParams(res, req.body, ['studentCode']);
    if (missField) 
        console.log("MISS_PARAMS");
    try {
        const {studentCode} = req.body;
        const checkCodeStudent = await Student.findOne({studentCode});
        if(!checkCodeStudent) {
            return send.fail(res, "UNVALID");
        } else {
            return send.success(res, "VALID");
        }
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG")
    }
}

exports.registerNewTeacher = async (req, res, next) => {
    const {name, email, dateOfBirth, gender, address, phone, description, classCode} = req.body;
    try {
        const newFormatDate = moment(dateOfBirth, 'DD-MM-YYYY').format();
        const PasswordHash = await ComparePassword.cryptPassword(req.body.password);
        const subject = await Subject.findOne({_id: req.body.subjectId});
        const data = {
            name,
            email,
            password: PasswordHash,
            gender,
            dateOfBirth: newFormatDate,
            address,
            phone,
            description,
            subjectId: subject,
            classCode
        }
        const newUser = await User.create({
            ...data,
            userType: "1",
        });
        await Teacher.create({
            userId: newUser._id,
            subjectId: subject,
            classCode: data.classCode
        });
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG");
    }
    return send.success(res, "REGISTER_SUCCESSFUL");
}

exports.registerNewParent = async (req, res, next) => {
    const {name, email, gender, dateOfBirth, address, phone, description, job, studentCode} = req.body;
    try {
        const newFormatDate = await moment(dateOfBirth, 'YYYY/MM/DD').format();
        const PasswordHash =  await ComparePassword.cryptPassword(req.body.password);
        const student = await Student.findOne({studentCode: req.body.studentCode});
        const data = {
            name,
            email,
            password: PasswordHash,
            gender,
            dateOfBirth: newFormatDate,
            address,
            phone,
            description,
            job,
            studentCode
        }
        const newUser = await User.create({
            ...data,
            userType: "2",
        });
        const parent = await Parent.create({
            job: data.job,
            studentCode: data.studentCode,
            userId: newUser._id
        });
        await Parent.update({_id: parent._id}, {
            $push: {
                studentId: student._id
            }
        })
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG");
    }
    return send.success(res, "REGISTER_SUCCESSFUL");
}

