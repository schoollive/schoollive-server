const config = require('../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const Story = require(config.ModelDir + '/Story');
const User = require(config.ModelDir + '/User');
const Reaction = require(config.ModelDir + '/Reaction');

exports.reactionStory = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    const postStory = await Story.findOne({_id: req.body.storyId})
    if(!postStory)
        return send.fail(res, "POST_NOT_EXISTS")
    try {
        const checkReaction = await Reaction.findOne({storyId: req.body.storyId})
        if(!checkReaction) {
            const user = await User.findOne({_id: req.tokenInfo._id});
            const data = {
                storyId: postStory._id,
                userId: user._id
            }
            await Reaction.create(data);
            const countNewReaction = await Reaction.find({storyId: req.body.storyId}).count();
            postStory.reactionNumbers = countNewReaction;
            postStory.isReaction = true;
            postStory.save();
            return send.success(res, "REACTION_SUCCESSFUL", countNewReaction)
        } else {
            await Reaction.find({_id: checkReaction._id}).remove();
            const countNewReaction = await Reaction.find({storyId: req.body.storyId}).count();
            postStory.reactionNumbers = countNewReaction;
            postStory.isReaction = false;
            postStory.save();
            return send.success(res, "REMOVE_REACTION_SUCCESSFUL")
        }
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}