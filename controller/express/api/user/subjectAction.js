const mongoose = require('mongoose');
const config = require('../../../../config/security');
const send = require(config.LibraryDir + '/send');

const Subject = require(config.ModelDir +  '/Subject');

exports.showListSubject = async (req, res, next) => {
    try {
        const listSubject = await Subject.find({});
        return send.success(res, "SUCCESSFUL", listSubject);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}