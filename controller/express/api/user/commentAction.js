const config = require('../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const Story = require(config.ModelDir + '/Story');
const Comment = require(config.ModelDir + '/Comment');
const User = require(config.ModelDir + '/User');

exports.commentStory = async (req, res, next) => {
    const {content} = req.body;
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    const postStory = await Story.findOne({_id: req.body.storyId})
    if(!postStory)
        return send.fail(res, "POST_NOT_EXISTS")
    try {
        const data = {
            content,
            userId: user._id,
            storyId: postStory._id
        }
        await Comment.create(data);
        const countNewComment = await Comment.find({storyId: req.body.storyId}).count();
        postStory.commentNumbers = countNewComment;
        postStory.save();
        return send.success(res, "COMMENT_SUCCESSFUL", countNewComment)
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG")
    }
}

exports.countComment = async (req, res, next) => {
    if(!req.tokenInfo || req.tokenInfo === undefined)
       return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const countComment = await Comment.find({storyId: req.body.storyId}).count();
        return send.success(res, "COUNT_SUCCESSFUL", countComment);
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}
exports.showComment = async (req, res, next) => {
    if(!req.tokenInfo || req.tokenInfo === undefined)
       return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const show = await Comment.find({storyId: req.body.storyId}).populate("userId", "username name")
        .exec()
        return send.success(res, "SHOW_SUCCESSFUL", show);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THINNG_WRONG")
    }
}