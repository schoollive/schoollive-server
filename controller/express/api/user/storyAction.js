const config = require('../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const Story = require(config.ModelDir + '/Story');
const Class = require(config.ModelDir + '/Class');
const Parent = require(config.ModelDir + '/Parent');
const Teacher = require(config.ModelDir + '/Teacher');
const Student = require(config.ModelDir + '/Student');
const Reaction = require(config.ModelDir + '/Reaction');
const Comment = require(config.ModelDir + '/Comment');

exports.postNewStory = async(req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    const {content} = req.body;
    try {
        if(user && user.userType === "1") {
            const teacher = await Teacher.findOne({userId: user._id})
            const teacherClassCode = teacher.classCode;
            const classCode = await Class.find({
                classCode:  {"$in" : teacherClassCode}, 
                _id: req.body.classId
            })
            if(classCode) {
                const data = {
                    content,
                    classId: req.body.classId,
                    userId: user._id
                }
                await Story.create(data);
                return send.success(res, "POST_SUCCESSFUL")
            }
        } else if (user && user.userType === "2") {
            const parent = await Parent.findOne({userId: user._id})
            const parentStudentCode = parent.studentId;
            const classCode = await Student.find({
                _id: {"$in" : parentStudentCode}, 
                classId: req.body.classId
            })
            .populate("classId", "name")
            if(classCode) {
                const data = {
                    content,
                    classId: req.body.classId,
                    userId: user._id
                }
                await Story.create(data);
                return send.success(res, "POST_SUCCESSFUL")
            }
        }
    } catch (e) {
        console.log(e)
        return send.fail(res, "POST_FAIL")
    }
}
exports.showStory = async(req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        let classIds =[];
        if(user.userType === "1") {
            const teacher = await Teacher.findOne({userId: user._id});
            const classCode = teacher.classCode;
            classIds = await Class.find({classCode: classCode})
        } else {
            const parent = await Parent.findOne({userId: user._id})
            .populate("studentId")
            .exec();
            const students = parent.studentId;
            students.forEach(student => {
                const classId = student.classId
                classIds.push(classId)
            })
        }
        const show = await Story.find({classId: {"$in" : classIds}})
        .lean()
        .populate("userId", "userType name gender avatar")
        .populate("classId", "name")
        const result = await Promise.all(show.map( async record => {
            const isReaction = await Reaction.exists({storyId: record._id, userId: user._id})
            if(isReaction) {
                return Object.assign({}, record, {isReaction: true})
            }
            return Object.assign({}, record, {isReaction: false})
            })
        )
        return send.success(res, "SHOW_SUCCESSFUL", result);
        
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG");
    }
}

exports.showMyHome = async(req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const show = await Story.find({userId: req.body.userId})
        .populate('userId', 'username')
        .exec();
        return send.success(res, "SHOW_SUCCESSFUL.", show)
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}