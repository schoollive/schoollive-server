const mongoose = require('mongoose');
const config = require('../../../../config/security');
const CheckPassword = require(config.LibraryDir + '/password');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const User = require(config.ModelDir + '/User');
const Teacher = require(config.ModelDir + '/Teacher');
const Parent = require(config.ModelDir + '/Parent');
const Student = require(config.ModelDir + '/Student');
const Class = require(config.ModelDir + '/Class');

exports.userLogin = (req, res) => {
    let missField = Common.checkMissParams(res, req.body, ['email', 'password'])
    let id;
    let email;
    if(missField) {
        console.log("MISS_PARAMS");
        return;
    } else {
        User.findOne({email: req.body.email})
            .then(user => {
                if(!user) {
                    return send.fail(res, "USER_NOT_EXIST")
                }
                id = user._id;
                userType = user.userType;
                username = user.username;
                email = user.email;
                let tokenPayload = {
                    _id: user._id,
                    userType: user.userType,
                    username: user.username,
                    email: user.email
                }
                return Promise.all([
                    CheckPassword.comparePassword(req.body.password, user.password),
                    Common.createToken(tokenPayload, "3 days"),
                ])
            })
            .then (result => {
                let isMatchPassword = result[0];
                let accessToken = result[1];
                    if(!isMatchPassword) {
                        return send.error(res, "WRONG_PASSWORD")
                }
                send.success(res, "LOGIN_SUCCESSFUL", {accessToken, userType, id, username, email})
            })
            .catch (err => {
                console.log(err)
                return send.fail(res, "SOME_THING_WRONG");
            }
        )
    }
}

exports.showInFoUser = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        let info;
        if(user.userType === '1') {
            info = await Teacher.findOne({userId: user._id}).populate("subjectId", "name")
            const showListClass = await Class.find({"classCode": info.classCode})
            info.classCode = showListClass
        } else {
            info = await Parent.findOne({userId: user._id})
            .lean();
            const students = await Student.find({"studentCode": info.studentCode})
            .populate("classId", "name")
            .lean()
            let classCode = students.map(s => s.classId)
            info = {
                ...info,
                classCode
            }
        }
        const data = {
            user,
            info
        }
        return send.success(res, "SUCCESSFUL", data);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}