const mongoose = require('mongoose');
const config = require('../../../../config/security');
const send = require(config.LibraryDir + '/send');
const ReportForm = require(config.ModelDir + '/ReportForm');
const ReportValue = require(config.ModelDir + '/ReportValue');

exports.showAllReporstForms = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const show = await ReportForm.find({})
        return send.success(res, "SUCCESSFUL", show);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e)
    }
} 

exports.showAllReporstPersonality = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const showReportPersonality = await ReportValue.find({
            studentId: req.body.studentId, 
            classId: req.body.classId, 
        })
        return send.success(res, "SUCCESSFUL", showReportPersonality)
    } catch(e) {
        console.log(e);
        return send.find(res, "SOME_THING_WRONG" + e)
    }
}