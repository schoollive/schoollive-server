const mongoose = require('mongoose');
const config = require('../../../../config/security');
const send = require(config.LibraryDir + '/send');

const Class = require(config.ModelDir +  '/Class');
const Teacher = require(config.ModelDir +  '/Teacher');
const Student = require(config.ModelDir +  '/Student');
const User = require(config.ModelDir +  '/User');
const Semester = require(config.ModelDir + '/Semester');
const StudentPlanner = require(config.ModelDir + '/StudentPlanner');
const ReportForm = require(config.ModelDir + '/ReportForm');
const ReportValue = require(config.ModelDir + '/ReportValue');

exports.areYouHomeRoomTeacher = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    const {homeRoomTeacherCode} = req.body;
    try {
        if(homeRoomTeacherCode) {
            const checkClassCode = await Teacher.findOne({
                userId: user._id, 
                classCode: { "$in" : [homeRoomTeacherCode]}
            });
            if(checkClassCode.homeRoomTeacherCode === null) {
                checkClassCode.homeRoomTeacherCode = homeRoomTeacherCode
                checkClassCode.save();
                return send.success(res, "VALID")
            }
            return send.fail(res, "UNVALID");
        }
        const teacher = await Teacher.findOne({userId: user._id})
        teacher.homeRoomTeacherCode === false;
        teacher.save();
        return send.success(res, "DO_NOT_HAVE_HOME_ROOM")
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG");
    }
}
exports.showAllMyClass = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const teacher = await Teacher.findOne({userId: req.tokenInfo._id});
        const myClass = teacher.classCode;
        const showClass = await Class.find({'classCode': myClass})
        return send.success(res, "SUCCESSFUL", showClass);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}

exports.showAllStudentOfClass = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const showStudent = await Student.find({classId: req.body.classId});
        return send.success(res, "SHOW_LIST_STUDENT_SUCCESSFUl", showStudent)
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}

// exports.reportOfSubject = async (req, res, next) => {
//     const {miniTest, bigTest, commentOfTeacher} = req.body;
//     if(!req.tokenInfo || req.tokenInfo === undefined)
//         return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
//     try {
//         const teacherReport = await Report.find({teacherId: req.tokenInfo._id});
//         const student = await Student.find({studentId: req.body._id})
//         const data = {
//             miniTest,
//             bigTest,
//             commentOfTeacher,
//             teacherId: teacherReport,
//             studentId: student
//         }
//         reportOfSubject.create(data);
//         return send.success(res, "SUCCESSFUL")
//     } catch (e) {
//         console.log(e)
//         return send.fail(res, "SOME_THING_WRONG")
//     }
// }

// exports.reportPersonality = async (req, res, next) => {
//     const {schoolYear, semester} = req.body;
//     if(!req.tokenInfo || req.tokenInfo === undefined)
//         return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
//     try {
//         const student = await Student.findOne({_id: req.body.studentId});
//         const studentName = student.name;
//         const clas = await Class.findOne({_id: req.body.classId});
//         const className = clas.name;
//         const teacher = await User.findOne({_id: req.tokenInfo._id});
//         const teacherName = teacher.name;
//         const data = {
//             schoolYear,
//             semester,
//             studentId: student,
//             teacherId: teacher,
//             classId: clas
//         }
//         await ReportPersonality.create({
//             ...data,
//             className: className,
//             teacherName: teacherName,
//             studentName: studentName
//         });
//         return send.success(res, "SUCCESSFUL");
//     } catch (e) {
//         console.log(e);
//         return send.fail(res, "SOME_THING_WRONG");
//     }
// }

exports.reportValue = async(req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const {schoolYear, semester} = req.body
        const student = await Student.findOne({_id: req.body.studentId});
        const studentName = student.name;
        const clas = await Class.findOne({_id: req.body.classId});
        const className = clas.name;
        const teacher = await User.findOne({_id: req.tokenInfo._id});
        const teacherName = teacher.name;
        const values = req.body.values;
        const report = [];
        for(let i = 0; i < values.length; i++) {
            const reportForm = await ReportForm.findOne({_id: values[i].reportFormId})
            const reportValue = await ReportValue.create({
                    schoolYear,
                    semester,
                    studentName,
                    className,
                    teacherName,
                    studentId: student._id,
                    classId: clas._id,
                    teacherId: teacher._id,
                    value: values[i].value,
                    commentOfTeacher: values[i].commentOfTeacher,
                    reportFormId: reportForm._id,
                    reportFormName: reportForm.name
            })
            report.push(reportValue)
        }
        return send.success(res, "SUCCESSFUL");
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}

exports.showSemesterOfSchool = async (req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const showSemester = await Semester.findOne({
            semester: req.body.semester, 
            classId: req.body.classId})
        if(!showSemester)
            return send.fail(res, "SEMESTER_DO_NOT_EXISTS");
        return send.success(res, "SUCCESSFUL", showSemester)
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}
// exports.studentPlanner = async (req, res, next) => {
//     const user = req.user
//     if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
//     try {
//         const studentPlanners = req.body.studentPlanners;
//         const teacher = req.user;
//         studentPlanners.forEach(element => {
//             const data = {
//                 classId: req.body.classId,
//                 teacherId: teacher._id,
//                 AreasOfLearning: element.AreasOfLearning,
//                 subjectId: element.subjectId,
//                 semesterId: element.semesterId
//             }
//             StudentPlanner.create(data);
//         })
//         return send.success(res, "SUCCESSFUL")
//     } catch(e) {
//         console.log(e);
//         return send.fail(res, "SOME_THING_WRONG" + e);
//     }
// }
exports.studentPlanner = async (req, res, next) => {
    const user = req.user;
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try{
        await StudentPlanner.create(req.body);
        return send.success(res, "SUCCESSFUL")
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e)
    }
}
// exports.studentPlanner = async (req, res, next) => {
//     const user = req.user;
//     if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
//     try {
//         req.body.teacherId = req.user._id
//         const weekOfSchoolLiveYear = req.body.weekOfSchoolLiveYear;
//         const teacher = req.user;
//         const timeTables = req.body.timeTables;
//         timeTables.forEach(element => {
//             const dateOfWeek = element.dateOfWeek;
//             const timeTable = element.timeTable;
//             timeTable.forEach(schedule => {
//                 const data = {
//                     subjectId: schedule.subjectId,
//                     AreasOfLearning: schedule.AreasOfLearning
//                 }
//                 StudentPlanner.create({
//                     ...data,
//                     weekOfSchoolLiveYear,
//                     teacherId: teacher._id,
//                     classId: req.body.classId,
//                     dateOfWeek: dateOfWeek,
//                 });
//             })
//         })
//         return send.success(res, "SUCCESSFUL")
//     } catch (e) {
//         console.log(e);
//         return send.fail(res, "SOME_THING_WRONG")
//     }
// }
exports.showStudentPlanner = async(req, res, next) => {
    const user = req.user
    if(!user) return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const studentPlanner = await StudentPlanner.find({
            classId: req.body.classId,
            teacherId: req.user._id
        })
        .populate("classId", "name")
        .populate("teacherId", "name")
        .populate("subjectId", "name")
        .populate("semesterId", "semester schoolOfYear")
        .clean();
        if(!studentPlanner)
            return send.fail(res, "DO_NOT_EXISTS");
        return send.success(res, "SUCCESSFULL", studentPlanner);
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}