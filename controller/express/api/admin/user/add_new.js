const mongoose = require('mongoose');
const config = require('../../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const School = require(config.ModelDir + '/School');
const Class = require(config.ModelDir + '/Class');
const Teacher = require(config.ModelDir + '/Teacher');
const Student = require(config.ModelDir + '/Student');
const Subject = require(config.ModelDir + '/Subject');

exports.addNewClass = async (req, res, next) => {
    const {name, startDate, endDate, numberOfTeachers, numberOfStudents, description} = req.body;
    if(!req.tokenInfo || req.tokenInfo === undefined)
        return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    let missField = Common.checkMissParams(res, req.body, ['name', 'startDate', 'endDate', 'numberOfTeachers', 'numberOfStudents', 'description'])
    if(missField)
        console.log("MISS_PARAM");
    try {
        const newClass = await School.findOne({schoolId: req.body._id}) 
        const data = {
            name,
            startDate,
            endDate,
            numberOfTeachers,
            numberOfStudents,
            description,
            schoolId: newClass
        }
        const addClass = await Class.create(data);
        addClass.randomCode();
        addClass.save();
        return send.success(res, "ADD_SUCCESSFUL");
    } catch (e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}

exports.addNewStudent = async (req, res, next) => {
    const {name, genre, dateOfBirth, address, subjectClass, subjectDescriptions} = req.body;
    if(!req.tokenInfo || req.tokenInfo === undefined)
       return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    let missField = Common.checkMissParams(res, req.body, ['name', 'genre', 'address', 'subjectClass', 'subjectDescription'])
    if(missField)
        console.log("MISS_PARAM");
    try {
        const data = {
            name,
            genre,
            dateOfBirth,
            address,
            subjectClass,
            subjectDescriptions
        }
        const newClass = await Class.findOne({classId: req.body._id})
        const addStudent = await Student.create
        ({
            ...data,
            classId: newClass
        });
        addStudent.randomCode();
        addStudent.save();
        return send.success(res, "ADD_SUCCESSFUL");
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG")
    }
}
exports.addNewSubject = async (req, res, next) => {
    const {name, description} = req.body;
    if(!req.tokenInfo || req.tokenInfo === undefined)
       return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    let missField = Common.checkMissParams(res, req.body, ['name', 'description'])
    if(missField)
        console.log("MISS_PARAM");
    try {
        const subjectOfTecher = await Teacher.findOne({teacherId: req.body._id});
        const inClass = await Class.findOne({classId: req.body._id});
        const data = {
            name,
            description,
            teacherId: subjectOfTecher,
            classId: inClass
        }
        await Subject.create(data);
        return send.success(res, "ADD_SUCCESSFUL")
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG")
    }
}