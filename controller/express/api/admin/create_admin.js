const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const ComparePassword = require(config.LibraryDir + '/password');
const send = require(config.LibraryDir + '/send');

const Admin = require(config.ModelDir + '/Admin');
const User = require(config.ModelDir + '/User');
const School = require(config.ModelDir + '/School');

exports.createNewAdmin = async (req, res, next) => {
    let missField = Common.checkMissParams(res, req.body, ['name', 'username', 'email', 'address', 'password', 'phone', 'numberOfClasses', 'numberOfTeachers', 'description'])
    if(missField)
        console.log("MISS_PARAM");
    const {name, username, email, address, phone, numberOfClasses, numberOfTeachers, description} = req.body;
    try {
        const PasswordHash = await ComparePassword.cryptPassword(req.body.password);
        const data = {
            name,
            username,
            email,
            address,
            password: PasswordHash,
            phone,
            numberOfClasses,
            numberOfTeachers,
            description,
        }
        const newAdmin = await Admin.create({
            userType: "0",
            username: data.username,
            name: data.name,
            email: data.email,
            password: data.password
        });
        const newSchool = await School.create({
            ...data,
            adminId: newAdmin._id
        });
        await newSchool.randomCode();
        newSchool.save();
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONg");
    }
    return send.success(res, "CREATE_SUCCESSFUL");
}