const mongoose = require('mongoose');
const config = require('../../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const ReportForm = require(config.ModelDir + '/ReportForm');

exports.createReportForm = async (req, res, next) => {
    const {name, group} = req.body;
    if(!req.tokenInfo || req.tokenInfo === undefined)
        return send.fail(res, "AUTHENTICAT_TOKEN_FAIL");
    let missField = Common.checkMissParams(res, req.body, ['name', 'group'])
    if(missField)
        console.log("MISS_PARAM");
    try {
        const data = {
            name,
            group
        }
        await ReportForm.create(data);
        console.log(data)
        return send.success(res, "SUCCESSFUL");
    } catch (e) {
        console.log(e)
        return send.fail(res, "SOME_THING_WRONG")
    }
}

