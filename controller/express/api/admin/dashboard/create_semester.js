const mongoose = require('mongoose');
const config = require('../../../../../config/security');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');
const moment = require('moment');

const Semester = require(config.ModelDir + '/Semester');
const Class = require(config.ModelDir + '/Class');

exports.createNewSemester = async (req, res, next) => {
    const {semester, schoolOfYear} = req.body;
    if(!req.tokenInfo || req.tokenInfo === undefined)
        return send.fail(res, "AUTHENTICATION_TOKEN_FAIL");
    try {
        const classOfSchool = await Class.findOne({_id: req.body.classId});
        if(!classOfSchool)
            return send.fail(res, "CLASS_DO_NOT_EXISTS")
        const data = {
            semester,
            schoolOfYear,
            classId: classOfSchool._id
        }
        Semester.create(data);
        return send.success(res, "SUCCESSFUL");
    } catch(e) {
        console.log(e);
        return send.fail(res, "SOME_THING_WRONG" + e);
    }
}