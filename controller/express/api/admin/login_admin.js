const mongoose = require('mongoose');
const config = require('../../../../config/security');
const CheckPassword = require(config.LibraryDir + '/password');
const Common = require(config.LibraryDir + '/common');
const send = require(config.LibraryDir + '/send');

const Admin = require(config.ModelDir + '/Admin');

exports.AdminLogin = (req, res) => {
    let missField = Common.checkMissParams(res, req.body, ['userType','email', 'password'])
    let id;
    let email;
    if(missField) {
        return send.fail(missField, "MISS_PARAM");
    } else {
        if(req.body.userType === '0') {
            Admin.findOne({email: req.body.email})
            .then(admin => {
                if(!admin) {
                    return send.fail(res, "ADMIN_NOT_EXISTS");
                }
                id = admin._id;
                userType = admin.userType;
                email = admin.email;
                let tokenPayload = {
                    _id: admin._id,
                    userType: admin.userType,
                    email: admin.email
                }
                return Promise.all([
                    CheckPassword.comparePassword(req.body.password, admin.password),
                    Common.createToken(tokenPayload, "3 days"),
                ])
            })
            .then(result => {
                let isMatchPassword = result[0];
                let accessToken = result[1];
                    if(!isMatchPassword) {
                        return send.error(res, "WRONG_PASSWORD")
                }
                send.success(res, "LOGIN_SUCCESSFUL", {accessToken, userType, id, email})
            })
            .catch (err => {
                console.log(err)
                return send.fail(res, "SOME_THING_WRONG");
            })
        }
    }
}
